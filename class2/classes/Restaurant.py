import requests
from geopy.geocoders import Nominatim


class Restaurant:
    def __init__(self, params):
        self.client_id = 'GSXE1IXLNWSHI01VBSP22UE4ERKSIXKTU3MWK1ESSUYOSAAP'
        self.client_secret = 'H0IGMZWHG4PBQXRVQQW1AXYVO0VS4PBALJJXLFDIL5NNHD30'
        self.params = params
        self.cordinates = self.get_cordinates()
        self.restaurants = self.get_restaurants()
      
    def get_cordinates(self):
        response = {}
        geolocator = Nominatim(user_agent="geopy")
        location = geolocator.geocode(self.params["location"])
        response['latitude'] = location.latitude
        response['longitude'] = location.longitude
        return response

    def get_restaurants(self):
        restaurants = []
        url_venues = (
            f'https://api.foursquare.com/v2/venues/search?'
            f'client_id={self.client_id}&'
            f'client_secret={self.client_secret}&'
            f'v=20130815&'
            f'll={self.cordinates["latitude"]}, {self.cordinates["longitude"]}&'
            f'query={self.params["keyword"]}'
        )
        request_venues = requests.get(url_venues)
        response_venues = request_venues.json()
        for result in response_venues['response']['venues'][:int(self.params['limit'])]:
            restaurant = {}
            restaurant['restaurant_name'] = result['name']
            restaurant['restaurant_adress'] = result['location']['formattedAddress']
            restaurant['image'] = self.get_photo(result['id'])
            restaurants.append(restaurant)
        return restaurants

    def get_photo(self, venue_id):
        url_photos = (
            f'https://api.foursquare.com/v2/venues/{venue_id}/photos?'
            f'client_id={self.client_id}&'
            f'client_secret={self.client_secret}&'
            f'v=20130815&'
        )
        request_photos = requests.get(url_photos)
        response_photos = request_photos.json()
        if response_photos['response']['photos']['items']:
            firstpic = response_photos['response']['photos']['items'][0]
            prefix = firstpic['prefix']
            suffix = firstpic['suffix']
            imageURL = prefix + self.params['image_size'] + suffix
        else:
            imageURL = "http://pixabay.com/get/8926af5eb597ca51ca4c/1433440765/cheeseburger-34314_1280.png?direct"
        return imageURL
