from flask import Flask, escape, request
from classes.Restaurant import Restaurant
import json
app = Flask(__name__)

@app.route('/class2', methods=['POST'])
def play():
    return json.dumps(Restaurant(request.json).restaurants)


if __name__ == '__main__':
    app.run(debug=True)
